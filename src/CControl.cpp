/*
 * cpprpn.cpp
 *
 *  Created on: Mar 7, 2016
 *      Author: maiko.costa
 */


#include "CCalculate.h"
#include "CConsoleIHM.h"
#include <iostream>

using namespace rpn;
using namespace std;

int main() {
  CCalculate r;
  CConsoleIHM i(&r);

  do {
    i.Get(": ");
    i.DisplayResult();

  } while (true);

  return 0;
}
