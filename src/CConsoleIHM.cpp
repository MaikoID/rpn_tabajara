/*
 * Input.cpp
 *
 *  Created on: Mar 7, 2016
 *      Author: maiko.costa
 */

#include "CConsoleIHM.h"

#include <iostream>
#include <boost/xpressive/xpressive.hpp>

namespace rpn {

using namespace std;
using namespace boost::xpressive;

CConsoleIHM::CConsoleIHM(CCalculate *r)
    : rpn(r) {
  cout << "\nReverse Polish Notation Calculator TABAJARA++" << endl;
  cout << "supported operations: +, -, /, *" << endl;
  cout << "supported operators: 10\t10+5i\t-10\t-10+5i\t+5i\t-1i\t10+1i" << endl;
  cout << "stack size: " << rpn->MAX_SIZE << endl;
}

void CConsoleIHM::DisplayResult() {
  cout << "-------------\n" << rpn->toString() << "\n-------------\n";
}

static bool replace_str(std::string& str, const std::string &from,
                        const std::string &to) {
  size_t start_pos = str.find(from);
  if (start_pos == std::string::npos)
    return false;
  str.replace(start_pos, from.length(), to);
  return true;
}

bool CConsoleIHM::Get(std::string prompt) {
  string in;
  cout << prompt;
  cout.flush();
  getline(cin, in);

  if (in.empty())
    return true;

  sregex rop = sregex::compile("([+-/*])");
  smatch mop;

  if (regex_match(in, mop, rop)) {
    char op = mop[1].str().data()[0];
    return rpn->Calculate(static_cast<CCalculate::Operations>(op));
  } else {
    sregex rc = sregex::compile("([+-]*[0-9.]+)?([+-]+[0-9.]*i)?");
    smatch mc;
    if (regex_match(in, mc, rc)) {
      string r = mc[1].str();
      string i = mc[2].str();
      replace_str(i, "i", "");

      rpn->SaveOperand(
          CComplex(stod(r.empty() ? "0" : r), stod(i.empty() ? "0" : i)));

      return true;
    }
  }
  return false;
}

} /* namespace cpprpn */
