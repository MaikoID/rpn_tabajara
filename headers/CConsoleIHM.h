/*
 * Input.h
 *
 *  Created on: Mar 7, 2016
 *      Author: maiko.costa
 */

#ifndef CCONSOLEIHM_H_
#define CCONSOLEIHM_H_

#include "CCalculate.h"
#include <string>

namespace rpn {

class CConsoleIHM {
 public:
  CConsoleIHM(CCalculate *r);
  void DisplayResult();

  bool Get(std::string prompt);
 private:
  CCalculate *rpn;

};

} /* namespace cpprpn */

#endif /* CCONSOLEIHM_H_ */
